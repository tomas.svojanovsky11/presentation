export const menuItems = [
    {
        label: "Co budu potřebovat?",
        link: "/what-do-i-need"
    },
    {
        label: "Doporučené",
        link: "/recommended"
    },
    {
        label: "O čem je frontend",
        link: "/what-is-frontend-about"
    },
    {
        label: "Jak funguje web",
        link: "/how-web-works"
    },
    {
        label: "Struktura HTML",
        link: "/html-structure"
    },
    {
        label: "Seznamy",
        link: "/lists"
    },
    {
        label: "Obrázky",
        link: "/images"
    },
    {
        label: "Blokové elementy",
        link: "/blocks"
    },
    {
        label: "Řádkové bloky",
        link: "/inlines"
    },
    {
        label: "Tabulky",
        link: "/tables"
    },
    {
        label: "Formuláře",
        link: "/forms"
    },
    {
        label: "Sémantické tagy",
        link: "/semantics"
    },
    {
        label: "Intro do CSS",
        link: "/css-intro"
    },
    {
        label: "Bootstrap",
        link: "/bootstrap"
    },
    {
        label: "Flask",
        link: "/flask"
    },
    {
        label: "Emmet",
        link: "/emmet"
    },
];
