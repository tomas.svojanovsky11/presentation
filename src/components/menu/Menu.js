import React from "react";
import { NavLink } from "react-router-dom";

import { MenuContainer } from "./menu.styled";
import { menuItems } from "./menuItems";

export function Menu () {
    return (
        <MenuContainer className="menu">
            <ol>
                {menuItems.map(({ link, label }) => (
                    <li key={link}>
                        <NavLink to={link}>{label}</NavLink>
                    </li>
                ))}
            </ol>
        </MenuContainer>
    );
}
