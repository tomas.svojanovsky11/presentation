import React from "react";

import { CodeStyled } from "./code.styled";

export function Code ({ children, showCopy = true }) {
    return (
        <CodeStyled>
            <code>
                {children}
            </code>

            {showCopy && <button
                className="button"
                onClick={() => {navigator.clipboard.writeText(children)}}
            >Copy</button>}
        </CodeStyled>
    );
}
