import styled from "styled-components";

export const CodeStyled = styled.pre`
  background: #f4f4f4;
  border: 1px solid #ddd;
  border-left: 3px solid #cfe2f3;
  color: #666;
  page-break-inside: avoid;
  font-family: monospace;
  font-size: 15px;
  line-height: 1.6;
  max-width: 100%;
  overflow: auto;
  padding: 1em;
  word-wrap: break-word;

  display: flex;
  justify-content: space-between;
  
  > code {
    display: flex;
    align-items: center;
  }
`;
