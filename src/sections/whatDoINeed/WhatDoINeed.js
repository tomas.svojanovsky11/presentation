import React from 'react';

export function WhatDoINeed(props) {
    return (
        <div>
            <h1>Co budu potřebovat</h1>

            <img width={500} src="images/funny.png" alt="Funny"/>

            <ol>
                <li>Prohlížec - Chrome</li>
                <li>Nodejs</li>
                <li>Git</li>
                <li>IDE - Webstorm, VSCode</li>
                <li>Plugin emmet</li>
            </ol>

            <h3>Windows/Linux/Mac</h3>
            <p>
                <a href="https://nodejs.org/en/" target="_blank">Nodejs</a>
            </p>

            <h3>Linux/Mac</h3>
            <p>
                <a href="https://github.com/nvm-sh/nvm" target="_blank">NVM</a>
            </p>
        </div>
    );
}
