import React from 'react';
import { Link } from "react-router-dom";

export function NotFound(props) {
    return (
        <div>
            <h1>Stránka nebyla nalezena</h1>

            <Link to="/">Úvodní stránka</Link>
        </div>
    );
}
