import React from 'react';

import { Code } from "../../components/code/Code";

export function Flask(props) {
    return (
        <div>
            <h1>Flask</h1>
            <p>Webový micro framework</p>

            <h1>Instalace</h1>

            <h3>Vytvoř virtuální prostředí</h3>

            <p>Mac</p>
            <code>python3 -m venv env</code>

            <br/>
            <br/>

            <p>Windows</p>
            <code>python3 -m venv env</code>

            <h3>Aktivace prostředí</h3>

            <p>Mac</p>
            <code>source ./env/bin/activate</code>

            <br/>
            <br/>

            <p>Windows</p>
            <code>.\env\Scripts\activate</code>

            <h3>Instalace flasku</h3>

            <div>
                <code>pip install flask</code>
            </div>

            <div>
                <code>
                    pip freeze > requirements.txt
                </code>
            </div>


            <h3>Gitignore</h3>
            <Code>{`env
/__pycache__
.DS_Store

.vscode
.idea`}</Code>

            <h3>Nastavení serveru</h3>

            <Code>
                {`from flask import Flask


app = Flask(__name__)


@app.route("/")
def index():
    return "Hello world"


if __name__ == "__main__":
    app.run(debug=True, port=8080, host="127.0.0.1")
`}
            </Code>

            <Code>
                {`return '''
        <h1>Pet application</h1>
        <button>Add pet</button>
    '''`}
            </Code>

            <h3>Vytvoření první šablony</h3>

            <Code>
                {`from flask import Flask, render_template`}
            </Code>

            // templates/index.html
            <Code>
                {`<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<section>
    <a href="#">Pet adoption</a>
</section>
<section>
    <a href="#">
        <div>
            <h2>Pet's name</h2>
            <p>Age</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et
                dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                ex ea commodo consequat.</p>
        </div>
    </a>
</section>
</body>
</html>`}
            </Code>

            <h3>Více stránek</h3>

            <Code>
                {`window.addEventListener('load', (event) => {
    // form radio buttons interactions
    const radioCircles = document.getElementsByClassName('fa-check-circle');

    for (let i = 0; i < radioCircles.length; i++) {
        radioCircles[i].addEventListener('click', ()=>{
            (radioCircles[i].previousSibling.previousSibling.checked == true) ? radioCircles[i].previousSibling.previousSibling.checked = false : radioCircles[i].previousSibling.previousSibling.checked = true;
        });
    };
});`}
            </Code>

            <Code>
                {`/* Project Styles */

:root {
    --primary: #272343;
    --secondary: #BAE8E8;
    --accent: #FFD803;
    --background: #FFFFFE;
}

* {
    font-family: 'Roboto', 'Arial', sans-serif;
    box-sizing: border-box;
}

body {
    background-color: var(--background);
}

h1,
h2,
h3,
h4,
h5,
h6,
p,
button,
a,
label,
legend {
    color: var(--primary);
}

a {
    text-decoration: none;
}

/* Header Styles */

.header i,
.card i {
    color: var(--secondary);
}

.main-header {
    text-align: center;
    margin-bottom: 5rem;
}

.main-header h1 {
    font-style: normal;
    font-weight: bold;
    font-size: 48px;
    line-height: 56px;
}

.main-header .add-pet-btn,
.main-header .back-btn,
#not-found a {
    font-style: normal;
    font-weight: bold;
    font-size: 24px;
    line-height: 28px;
    background: var(--accent);
    border-radius: 20px;
    border: none;
    width: 143px;
    height: 47px;
}

.main-header .add-pet-btn,
.main-header .back-btn {
    padding: 10px 15px 10px 15px;
}

.main-header .add-pet-btn i.fa-plus {
    font-style: normal;
    font-weight: 900;
    font-size: 18px;
    line-height: 21px;
    position: relative;
    bottom: 2px;
    margin-right: 5px;
}

/* Homepage - Card Style */

.card {
    margin: auto;
    position: relative;
    box-shadow: 0px 2px 20px rgba(0, 0, 0, 0.1);
    width: 292px;
    height: 330px;
    margin-bottom: 71px;
    border-radius: 20px;
}

.card .fas {
    color: var(--accent);
}

.card img {
    width: 292px;
    height: 205px;
    border-radius: 20px 20px 0px 0px;
}

.card button {
    position: absolute;
    right: 5px;
    top: 5px;
    background: none;
    border: none;
    font-style: normal;
    font-weight: normal;
    font-size: 24px;
    line-height: 28px;
}

.card h2 {
    font-style: normal;
    font-weight: normal;
    font-size: 24px;
    line-height: 28px;
    text-align: left;
    display: inline-block;
    margin-right: 88px;
    margin-left: 32px;
    margin-top: 18px;
    padding-bottom: 5px;
    border-bottom: 2px solid var(--secondary);
}

.card h3 {
    font-style: normal;
    font-weight: normal;
    font-size: 18px;
    line-height: 21px;
    display: inline-block;
}

.card p {
    font-style: normal;
    font-weight: normal;
    font-size: 14px;
    line-height: 16px;
    display: block;
    margin-left: 32px;
    margin-top: 0px;
    margin-bottom: 15px;
    width: 228px;
    height: 40px;
    overflow: hidden;
}

/* Form Styles */

.main-header button i.fa-caret-left {
    margin-right: 3px;
}

#form {
    width: 90%;
    margin: auto;
    margin-top: 40px;
}

#form .form-container {
    width: fit-content;
    margin: auto;
}

#form .form-container input {
    display: block;
    height: 61px;
    font-style: normal;
    font-weight: normal;
    font-size: 24px;
    line-height: 28px;
    border: 2px solid var(--secondary);
    border-radius: 20px;
    margin-bottom: 40px;
    padding-left: 20px;
    margin-top: 15px;
}

#form .form-container label {
    font-style: normal;
    font-weight: normal;
    font-size: 24px;
    line-height: 28px;
    width: 80px;
    margin-bottom: 10px;
    border-bottom: 2px solid var(--secondary);
}

#form fieldset {
    border: none;
    margin-bottom: 30px;
}

#form fieldset div {
    text-align: center;
}

#form legend {
    font-style: normal;
    font-weight: normal;
    font-size: 24px;
    line-height: 28px;
    text-align: center;
    background-color: var(--secondary);

    padding: 10px;
    border-radius: 20px;
}

#form fieldset input {
    visibility: hidden;
    width: 0px;
    height: 0px;
}

#form fieldset i {
    color: var(--primary);
    font-size: 24px;
    margin-right: 10px;
    margin-top: 20px;
}

#form fieldset input[type='radio']:checked + i {
    color: var(--accent);
}

#form fieldset label {
    font-style: normal;
    font-weight: normal;
    font-size: 24px;
    line-height: 28px;
    margin-right: 15px;
}

#form textarea {
    margin-top: 20px;
    width: 85%;
    border: 2px solid var(--secondary);
    border-radius: 20px;
    font-style: normal;
    font-weight: normal;
    font-size: 24px;
    line-height: 28px;
    padding: 20px;
}

#form #form-buttons {
    text-align: center;
    margin-top: 80px;
    margin-bottom: 50px;
}

#form button {
    width: 143px;
    height: 47px;
    background: none;
    border-radius: 20px;
    margin: auto;
    font-style: normal;
    font-weight: bold;
    font-size: 24px;
    line-height: 28px;
}

#form button[type="reset"] {
    border: 2px solid var(--accent);
    margin-right: 27px;
}

#form button[type="submit"] {
    border: none;
    background-color: var(--accent);
}

/* Pet Styles */

#pet-data {
    text-align: center;
    margin: auto;
    margin-bottom: 100px;
    width: 90%;
}

#pet-data img {
    margin: auto;
    width: 242px;
    height: 242px;
    border-radius: 20px;
    margin-top: 40px;
    margin-bottom: 20px;
    display: block;
    box-shadow: 0px 2px 20px rgba(0, 0, 0, 0.25);
    object-fit: cover;
}

#pet-data div {
    text-align: left;
}

#pet-data #pet-name {
    text-align: center;
}

#pet-data h2 {
    font-style: normal;
    font-weight: bold;
    font-size: 28px;
    line-height: 28px;
    border-bottom: 2px solid var(--secondary);
    width: auto;
    display: inline-block;
    padding: 10px;
}

#pet-data p {
    font-style: normal;
    font-weight: normal;
    font-size: 20px;
    line-height: 28px;
    display: inline-block;
    margin-left: 40px;
}

#pet-data #description p {
    display: block;
    text-align: left;
    margin-left: 0px;
    font-style: normal;
    font-weight: normal;
    font-size: 20px;
    line-height: 21px;
}

#edit-delete {
    text-align: center;
    margin-bottom: 40px;
}

#edit-delete a {
    text-decoration: none;
    font-size: 24px;
    color: var(--primary);
}

#edit-delete a i {
    margin-right: 5px;
}

#edit-delete a:first-child {
    margin-right: 50px;
}

#edit-delete a:hover{
    color: var(--accent);
}

/* 404 page */

#not-found {
    text-align: center;
    margin-top: 100px;
}

#not-found img {
    border-radius: 20px;
}

#not-found p {
    margin-bottom: 100px;
}

@keyframes bounce {
    0% {
        transform: translate3d(0,0,0);
    }
    10% {
        transform: translate3d(0,-200px,0);
    }
    30% {
        transform: translate3d(0,0,0);
    }
    40% {
        transform: translate3d(0,-100px,0);
    }
    60% {
        transform: translate3d(0,0,0);
    }
    70% {
        transform: translate3d(0,-50px,0);
    }
    75% {
        transform: translate3d(0,0,0);
    }
    85% {
        transform: translate3d(0,-25px,0);
    }
    90% {
        transform: translate3d(0,0,0);
    }
    95% {
        transform: translate3d(0,-10px,0);
    }
    100% {
        transform: translate3d(0,0,0);
    }
}

#not-found h2 i {
    margin-left: 20px;
    color: var(--accent);
    animation: bounce 1.5s cubic-bezier(.5,0.05,1,.5);
    animation-iteration-count: 1;
    animation-direction: alternate;
}

#not-found a {
    text-decoration: none;
    width: 200px;
    height: 60px;
    padding: 10px 15px;
}

#not-found .fa-bone {
    font-size: 14px;
    transform: translateX(-12px) translateY(10px) rotate(135deg);
}



/* Media Queries */

@media (min-width: 550px){

    /* Pet Page */

    #pet-data {
        width: 80%;
    }
}

/* iPad */

@media (min-width: 768px){

    /* Form Page */
    #form fieldset {
        margin: auto;
        margin-bottom: 30px;
        width: 50%;
    }

    /* Pet Page */

    #pet-data {
        width: 50%;
    }

    #pet-data img {
        width: 400px;
        height: 400px;
    }
}

/* Desktops and Laptops */

@media (min-width: 1024px){

    .main-header h1 {
        display: inline-block;
        margin-right: 100px;
    }

    /* Homepage Page */

    #filter #filter-button {
        display: none;
    }

    #filter .filters {
        display: inline-block;
    }

    #filter .filters {
        margin-bottom: 50px;
    }

    #pet-cards {
        width: 80%;
        margin: auto;
        text-align: center;
    }

    #pet-cards .card {
        display: inline-block;
    }

    #pet-cards .card {
        margin-right: 50px;
    }

    /* Form Page */

    #form .columns {
        width: 75%;
        margin: auto;
        text-align: center;
    }

    #form .form-container {
        text-align: left;
    }

    #form .col1 {
        margin-right: 50px;
    }
    #form fieldset {
        width: 100%;
    }

    #form #descr{
        width: 75%;
    }

    /* Pet Page */

    #pet-data {
        width: 65%;
    }

    #pet-data #left-col,
    #pet-data #right-col {
        display: inline-block;
    }

    #pet-data #right-col {
        margin-left: 60px;
    }

    #pet-data #description {
        width: 650px;
        margin: auto;
    }
}

/* Desktop Screens */

@media (min-width: 1224px){

    /* Form Page */

    #form .col1, #form .col2 {
        display: inline-block;
    }

    /* Headers */

    .main-header h1 {
        margin-right: 400px;
    }
}

/* Large Screens */

@media (min-width: 1824px){

    .main-header h1 {
        margin-right: 600px;
    }

    /* Form Page */

    #form #descr {
        width: 45%;
    }

    /* Pet Page */

    #pet-data #right-col {
        margin-left: 100px;
    }
}`}
            </Code>

            // add pet
            <Code>
                {`<!DOCTYPE HTML>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Pet Adoption</title>
        <meta name="description" content="A collection of adoptable pets.">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <script src="https://kit.fontawesome.com/75bde6854e.js" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
    </head>
    <body>
        <section class="main-header">
            <a href="#">
                <h1 class="header">
                    <i class="fas fa-paw"></i>
                    Pet Adoption
                </h1>
            </a>
            <a class="back-btn" href="#">
                <i class="fas fa-caret-left"></i>
                Back
            </a>
        </section>
        <section id="pet-cards">
            <form action="#">
                <div class="columns">
                    <div class="col1">
                        <div class="form-container">
                            <label for="name">Name</label>
                            <input id="name" name="name" type="text" placeholder="Fuzzy Wuzzy">
                        </div>
                        <div class="form-container">
                            <label for="age">Age</label>
                            <input id="age" name="age" type="text" placeholder="6 months">
                        </div>
                        <div class="form-container">
                            <label for="breed">Breed</label>
                            <input id="breed" name="breed" type="text" placeholder="American Shorthair">
                        </div>
                        <div class="form-container">
                            <label for="url"><i class="fas fa-camera"></i> URL</label>
                            <input id="url" name="url" type="text" placeholder="Photo link">
                        </div>
                    </div>
                    <div class="col2">
                        <div class="form-container">
                            <label for="color">Color</label>
                            <input id="color" name="color" type="text" placeholder="Grey">
                        </div>
                        <div class="form-container">
                            <label for="size">Size</label>
                            <input id="size" name="size" type="text" placeholder="Medium">
                        </div>
                        <div class="form-container">
                            <label for="weight">Weight</label>
                            <input id="weight" name="weight" type="text" placeholder="5lbs">
                        </div>
                        <div class="form-container">
                            <label for="alt">Image Alt Text</label>
                            <input id="alt" name="alt" type="text" placeholder="This photo is...">
                        </div>
                    </div>
                </div>
                <div class="columns">
                    <div class="col1">
                        <fieldset>
                            <div>
                                <legend>Pet Type</legend>
                                <input id="dog" type="radio" name="pet" value="Dog">
                                <i class="fas fa-check-circle"></i>
                                <label for="dog">Dog</label>
                                <input id="cat" type="radio" name="pet" value="Cat">
                                <i class="fas fa-check-circle"></i>
                                <label for="cat">Cat</label>
                                <input id="other" type="radio" name="pet" value="Other">
                                <i class="fas fa-check-circle"></i>
                                <label for="other">Other</label>
                            </div>
                        </fieldset>
                        <fieldset>
                            <div>
                                <legend>Gender</legend>
                                <input id="female" type="radio" name="gender" value="Female">
                                <i class="fas fa-check-circle"></i>
                                <label for="female">Female</label>
                                <input id="male" type="radio" name="gender" value="Male">
                                <i class="fas fa-check-circle"></i>
                                <label for="male">Male</label>
                            </div>
                        </fieldset>
                    </div>
                    <div class="col2">
                        <fieldset>
                            <div>
                                <legend>Spayed/Neutered</legend>
                                <input id="true" type="radio" name="spay" value="Yes">
                                <i class="fas fa-check-circle"></i>
                                <label for="true">Yes</label>
                                <input id="false" type="radio" name="spay" value="No">
                                <i class="fas fa-check-circle"></i>
                                <label for="false">No</label>
                                <input id="na" type="radio" name="spay" value="Unknown">
                                <i class="fas fa-check-circle"></i>
                                <label for="na">Unkown</label>
                            </div>
                        </fieldset>
                        <fieldset>
                            <div>
                                <legend>Housetrained</legend>
                                <input id="yes" type="radio" name="housetrained" value="Yes">
                                <i class="fas fa-check-circle"></i>
                                <label for="yes">Yes</label>
                                <input id="no" type="radio" name="housetrained" value="No">
                                <i class="fas fa-check-circle"></i>
                                <label for="no">No</label>
                                <input id="unkown" type="radio" name="housetrained" value="Unknown">
                                <i class="fas fa-check-circle"></i>
                                <label for="unkown">Unkown</label>
                            </div>
                        </fieldset>
                    </div>
                </div>
                <fieldset id="descr">
                    <div>
                        <legend>Description</legend>
                        <label for="description" style="display: none;">Description</label>
                        <textarea name="description" id="description" cols="30" rows="10" placeholder="This pet is..."></textarea>
                    </div>
                </fieldset>
                <div id="form-buttons">
                    <button type="reset">Cancel</button>
                    <button type="submit">Submit</button>
                </div>
            </form>
        </section>
    </body>
</html>`}
            </Code>

            // pet
            <Code>
                {`<!DOCTYPE HTML>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Pet Adoption</title>
        <meta name="description" content="A collection of adoptable pets.">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <script src="https://kit.fontawesome.com/75bde6854e.js" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
    </head>
    <body>
        <section class="main-header">
            <a href="#">
                <h1 class="header">
                    <i class="fas fa-paw"></i>
                    Pet Adoption
                </h1>
            </a>
            <a class="add-pet-btn" href="#" alt="Add Pet">
                <i class="fas fa-plus"></i>
                Pet
            </a>
        </section>
        <section id="pet-data">
            <img src="https://cdn.pixabay.com/photo/2019/08/19/07/45/dog-4415649_960_720.jpg" alt="Fluff the corgi">
            <div id="pet-name">
                <h2>Name</h2>
                <p>Fluff</p>
            </div>
            <div id="left-col">
                <div>
                    <h2>Age</h2>
                    <p>5 years</p>
                </div>
                <div>
                    <h2>Breed</h2>
                    <p>Corgi</p>
                </div>
                <div>
                    <h2>Color</h2>
                    <p>Brown</p>
                </div>
                <div>
                    <h2>Size</h2>
                    <p>Small</p>
                </div>
            </div>
            <div id="right-col">
                <div>
                    <h2>Weight</h2>
                    <p>35lbs</p>
                </div>
                <div>
                    <h2>Gender</h2>
                    <p>Male</p>
                </div>
                <div>
                    <h2>Spayed/Neutered</h2>
                    <p>Yes</p>
                </div>
                <div>
                    <h2>Housetrained</h2>
                    <p>Yes</p>
                </div>
            </div>
            <div id="description">
                <h2>Description</h2>
                <p>This is a great dog. The best dog in all the world</p>
            </div>
        </section>
        <section id="edit-delete">
            <a href="#"><i class="far fa-edit"></i>Edit</a>
            <a href="#"><i class="far fa-trash-alt"></i>Delete</a>
        </section>
    </body>
</html>`}
            </Code>


            // index.html
            <Code>
                {`<!DOCTYPE HTML>
            <html lang="en">
            <head>
                <meta charset="utf-8">
                    <title>Pet Adoption</title>
                    <meta name="description" content="A collection of adoptable pets.">
                        <meta name="viewport" content="width=device-width, initial-scale=1">

                            <script src="https://kit.fontawesome.com/75bde6854e.js" crossorigin="anonymous"></script>
                            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
            </head>
            <body>
            <section class="main-header">
                <a href="#">
                    <h1 class="header">
                        <i class="fas fa-paw"></i>
                        Pet Adoption
                    </h1>
                </a>
                <a class="add-pet-btn" href="#" alt="Add Pet">
                    <i class="fas fa-plus"></i>
                    Pet
                </a>
            </section>
            <section id="pet-cards">
                <a href="#">
                    <div class="card">
                        <img src="https://cdn.pixabay.com/photo/2019/08/19/07/45/dog-4415649_960_720.jpg" alt="Dog picture">
                            <h2>Name</h2>
                            <h3>6 months</h3>
                            <p>This is a great dog. The best dog in all the world.</p>
                    </div>
                </a>
                <a href="#">
                    <div class="card">
                        <img src="https://cdn.pixabay.com/photo/2019/08/19/07/45/dog-4415649_960_720.jpg" alt="Dog picture">
                            <h2>Name</h2>
                            <h3>6 months</h3>
                            <p>This is a great dog. The best dog in all the world.</p>
                    </div>
                </a>
                <a href="#">
                    <div class="card">
                        <img src="https://cdn.pixabay.com/photo/2019/08/19/07/45/dog-4415649_960_720.jpg" alt="Dog picture">
                            <h2>Name</h2>
                            <h3>6 months</h3>
                            <p>This is a great dog. The best dog in all the world.</p>
                    </div>
                </a>
                <a href="#">
                    <div class="card">
                        <img src="https://cdn.pixabay.com/photo/2019/08/19/07/45/dog-4415649_960_720.jpg" alt="Dog picture">
                            <h2>Name</h2>
                            <h3>6 months</h3>
                            <p>This is a great dog. The best dog in all the world.</p>
                    </div>
                </a>
                <a href="#">
                    <div class="card">
                        <img src="https://cdn.pixabay.com/photo/2019/08/19/07/45/dog-4415649_960_720.jpg" alt="Dog picture">
                            <h2>Name</h2>
                            <h3>6 months</h3>
                            <p>This is a great dog. The best dog in all the world.</p>
                    </div>
                </a>
            </section>
            </body>
            </html>`}
            </Code>
        </div>
    );
}
