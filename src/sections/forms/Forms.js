import React from 'react';

import { Code } from "../../components/code/Code";

export function Forms(props) {
    return (
        <main>
            <h1>Formuláře</h1>

            <h3>Formuláře slouží k interakci s uživatelem v rámci konkrétní webové stránky. Např. přihlášení, změna profilových dat, změna obrázku atd.</h3>

            <div>
                <form action="" method="post">
                    <div>
                        <label htmlFor="email">Email:</label>
                        <input type="email" name="email" id="email" required/>
                    </div>

                    <div>
                        <label htmlFor="password">Heslo:</label>
                        <input type="password" name="password" id="password" required/>
                    </div>

                    <button type="submit">Submit</button>
                </form>
            </div>

            <section>
                <h3>Příklad</h3>

                <Code>
                    {`<form action="" method="post">
    <div>
        <label for="email">Email:</label>
        <input type="email" name="email" id="email" required>
    </div>

    <div>
        <label for="password">Heslo:</label>
        <input type="password" name="password" id="password" required>
    </div>
    
    <button type="submit">Submit</button>
</form>`}
                </Code>
            </section>

            <section>
                <h3>Form</h3>

                <Code>
                    {`<form></form>`}
                </Code>
            </section>

            <section>
                <h3>Input</h3>

                <h4>Text</h4>
                <div>
                    <input type="text" placeholder="Do you like cats?"/>
                </div>
                <Code>
                    {`<input type="text" placeholder="Do you like cats?">`}
                </Code>

                <h4>Checkbox</h4>

                <div>
                    <input type="checkbox" />
                </div>
                <div>
                    <input type="checkbox" checked/>
                </div>
                <Code>
                    {`<input type="checkbox" checked>`}
                </Code>

                <h4>Number</h4>
                <div>
                    <input type="number"/>
                </div>

                <Code>
                    {`<input type="number">`}
                </Code>

                <h4>Radio</h4>
                
                <div>
                    <input type="radio" value="cats" name="animal"/>
                    <input type="radio" value="dogs" name="animal"/>
                </div>
                <Code>
                    {`<input type="radio">`}
                </Code>

                <h4>Password</h4>
                <div>
                    <input type="password"/>
                </div>
                <Code>
                    {`<input type="password">`}
                </Code>

                <h4>File</h4>
                <input type="file"/>
                <Code>
                    {`<input type="file">`}
                </Code>

                <h4>Email</h4>
                <input type="email"/>
                <Code>
                    {`<input type="email">`}
                </Code>
            </section>

            <section>
                <h3>Textarea</h3>
                <textarea rows="5" cols="20"></textarea>

                <Code>
                    {`<textarea rows="5" cols="20"></textarea>`}
                </Code>
            </section>

            <section>
                <h3>Select</h3>

                <div>
                    <label htmlFor="animals">Animals:</label><br/>
                    <select id="animals">
                        <option value="cats">Cats</option>
                        <option value="dogs">Dogs</option>
                        <option value="rabbits">Rabbits</option>
                        <option value="mice">Mice</option>
                    </select>
                </div>

                <Code>
                    {`<div>
    <label htmlFor="animals">Animals:</label><br/>
    <select id="animals">
        <option value="cats">Cats</option>
        <option value="dogs">Dogs</option>
        <option value="rabbits">Rabbits</option>
        <option value="mice">Mice</option>
    </select>
</div>`}
                </Code>
            </section>

            <section>
                <h3>Button</h3>

                <div>
                    <button type="submit">Submit</button>
                </div>

                <Code>
                    {`<button type="submit">Submit</button>`}
                </Code>
            </section>
        </main>
    );
}
