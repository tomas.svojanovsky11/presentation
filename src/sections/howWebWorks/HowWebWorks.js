import React from "react"

export function HowWebWorks () {
    return (
        <div>
            <h1>Jak funguje web</h1>

            <div>
                <img width={1000} src="images/how_web_works.webp" alt="how does web work"/>
            </div>

            <a href="https://www.youtube.com/watch?v=hJHvdBlSxug" target="_blank">Zdroj</a>
        </div>
    )
}
