import React from 'react';

import { Code } from "../../components/code/Code";

export function CssIntro(props) {
    return (
        <div>
            <h1>CSS Intro</h1>
            <p>Vybíráme značky nebo atributy a jim přiřazujeme vlasnosti.</p>
            <p>Atribut - Dodatečná informace u značky</p>

            <h3>Připojení CSS</h3>
            <p>Abysme mohli CSS používat je potřeba ho připojit do HTML</p>

            <Code>
                {`div style="background-color: red;>Some text</div>"`}
            </Code>

            <Code>
                {`<head>
    <style>
    Moje css
    </style>
</head>`}
            </Code>

            <Code showCopy={false}>Externí soubor</Code>


            <h3>Pomocí tagu</h3>

            <Code>
                {`<div>Some text</div>
<style>
    div {
        color: red;
    }
</style>`}
            </Code>

            <h2>Pomocí id</h2>

            <Code>
                {`<div id="some-id">Some text</div>
<style>
    #some-id {
        color: red;
    }
</style>`}
            </Code>

            <h3>Pomocí tříd</h3>

            <Code>
                {`<div class="some-class">Some text</div>
<style>
    .some-class {
        color: red;
    }
</style>`}
            </Code>


            <h3>Specificita</h3>

            <img width={600} src="images/specifishity.png" alt="Specificity"/>
            <img width={600} src="images/specifishity2.png" alt="Specificity 2"/>
        </div>
    );
}
