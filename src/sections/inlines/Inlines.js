import React from 'react';

import { Code } from "../../components/code/Code";

export function Inlines(props) {
    return (
        <div>
            <h1>Řádkové elementy elementy</h1>
            <p>Pokud dáme dva a více řádkových elementů za sebe, tak budou na stejném řádku.</p>
            
            <section>
                <h3>span</h3>

                <span>Some text 1</span>
                <span> </span>
                <span>Some text 2</span>

                <Code>
                    {`<span>Some text 1</span>
<span> </span>
<span>Some text 2</span>`}
                </Code>
                
                <h3>b</h3>

                <b>Some text 1</b>
                <span> </span>
                <b>Some text 2</b>

                <Code>
                    {`<b>Some text 1</b>
<span> </span>
<b>Some text 2</b>`}
                </Code>
                
                <h3>i</h3>

                <i>Some text 1</i>
                <span> </span>
                <i>Some text 2</i>

                <Code>
                    {`<i>Some text 1</i>
<span> </span>
<i>Some text 2</i>`}
                </Code>
            </section>
        </div>
    );
}
