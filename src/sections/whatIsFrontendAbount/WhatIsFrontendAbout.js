import React from 'react';

import { Code } from "../../components/code/Code";

export function WhatIsFrontendAbout(props) {
    return (
        <div>
            <h1>O čem je frontend?</h1>

            <img width={800} src="images/frontend_flow.png" alt="Frontend flow"/>

            <h3>O jaký typ aplikace se jedná</h3>

            <ul>
                <li>Single page aplikace</li>
                <li>Server side rendering</li>
                <li>Multipage aplikace</li>
                <li>Mobilní aplikace, desktopová aplikace</li>
            </ul>

            <h3>Rozhodnotě se v čem to budete psát</h3>

            <p>Většina frontendových aplikací se píše v javascriptu. Pokud potřebujete performance můžete si zvolit webassembly.</p>
            <p>Multipage aplikace - Flask, Express...</p>

            <ul>
                <li>
                    <a href="https://reactjs.org/" target="_blank">React</a>
                </li>
                <li>
                    <a href="https://angular.io/" target="_blank">Angular</a>
                </li>
                <li>
                    <a href="https://v3.vuejs.org/" target="_blank">Vue</a>
                </li>

                <li>
                    <a href="https://v3.vuejs.org/" target="_blank">Svelte</a>
                </li>
            </ul>

            <h3>Je dobré si rozmyslet strukturu aplikace</h3>

            <p>V pozdější fází aplikaci vede k nekonzistenci a můžou se špatně dohledávat věci.</p>

            <h3>Zjistit jak bude probíhat komunikace s backendem</h3>

            <p>Tady s tím toho moc neuděláte.</p>
            <p>Dokumentace - swagger atd.</p>

            <h3>Dobře sestavit komponenty</h3>

            <p>Komponenta - zapouzdřená logika např. tlačítko, container atd.</p>

            <h3>Nezapomínat na testy</h3>

            <p>e2e i unity testy</p>

            <h3>Prettier, linter</h3>

            <p>Kvalita kódu</p>

            <h3>Další věci</h3>

            <p>Store, pwa, react query, docker, git hooky, CI</p>

            <Code>
                {`import React from "react";

export type ButtonType = "submit" | "reset" | "button" | undefined;

interface ButtonProps {
  kind?:
    | "primary"
    | "secondary"
    | "tertiary"
    | "link"
    | "productSelected"
    | "productUnselected";
  size?: "large" | "small";
  fullWidth?: boolean;
  disabled?: boolean;
  label: React.ReactNode | string;
  className?: string;
  children?: React.ReactNode;
  type?: ButtonType;
  onClick?: () => void;
}

export const Button = ({
  kind = "primary",
  size = "large",
  fullWidth,
  className,
  label,
  children,
  type = "button",
  ...props
}: ButtonProps) => {
  return (
    <button
      type={type}
      className={[
        "c-btn",
        \`c-btn--\${kind}\`,
        \`c-btn--\${size}\`,
        fullWidth ? "c-btn--fullwidth" : undefined,
        className,
      ]
        .filter(Boolean)
        .join(" ")}
      {...props}
    >
      {label}
      {children}
    </button>
  );
};`}
            </Code>
        </div>
    );
}
