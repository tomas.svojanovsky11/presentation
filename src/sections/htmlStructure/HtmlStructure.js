import React from "react"

import { Code } from "../../components/code/Code";

export function HtmlStructure () {
    return (
        <div>
            <h1>HTML</h1>
            <p>HTML je značkovací jazyk pro tvorbu webových strnánek, které jsou propojeny hypertextovými odkazy.</p>
            <p>Vznikl někdy v roce 1989 - 1990.</p>

            <div>
                <a href="https://html.spec.whatwg.org/">Specifikace</a>
            </div>

            <h3>Struktura dokumentu</h3>

            <Code>
                {`1. <!DOCTYPE HTML>
2. <html lang="en">
3. <head>
4.     <meta charset="UTF-8">
5.     <meta name="viewport"
6.           content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
7.     <meta http-equiv="X-UA-Compatible" content="ie=edge">
8.     <title>Document</title>
9. </head>
10 .<body>
11.
12. </body>
13. </html>`}
            </Code>

            <p>Máme dva druhy tagů - párový a nepárový</p>


            <h3>1. DTD direktiva</h3>

            <section>
                <p>Deklarace typu dokumentu</p>
                <p>HTML v4 - {`<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">`}</p>
            </section>
        </div>
    )
}
