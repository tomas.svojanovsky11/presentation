import React from 'react';

import { Code } from "../../components/code/Code";

export function Blocks(props) {
    return (
        <div>
            <h1>Blokové elementy</h1>
            <p>Pokud dáme dva a více blokových elementů za sebe, tak každý bude na novém řádku. Zalamují se.</p>

            <section>
                <h3>p</h3>

                <p>Paragraph</p>

                <Code>
                    {`<p>Paragraph</p>`}
                </Code>

                <h3>br</h3>

                <div>This is my cat.</div>
                <br/>
                <div>This is my dog</div>

                <Code>
                    {`<div>This is my cat.</div>
<br>
<div>This is my dog</div>
`}
                </Code>

                <h3>div</h3>

                <Code>
                    {`<div>Some text</div>`}
                </Code>

                <h3>h1-h6</h3>

                <h1>Title level one</h1>

                <Code>
                    {`<h1>Title level one</h1>`}
                </Code>

                <h3>address</h3>

                <address>
                    <a href="mailto:tomas@gmail.com">tomas@gmail.com</a><br/>
                    <a href="tel:+420123456789">+420 123456789</a>
                </address>

                <Code>
                    {`<address>
  <a href="mailto:tomas@gmail.com">tomas@gmail.com</a><br>
  <a href="tel:+420123456789">+420 123456789</a>
</address>`}
                </Code>

                <h3>hr</h3>

                <hr/>

                <Code>
                    {`<hr/>`}
                </Code>

                <h3>blockquote</h3>

                <figure>
                    <blockquote cite="https://www.shopify.com/blog/motivational-quotes">
                        <p>Don’t let yesterday take up too much of today.</p>
                    </blockquote>
                    <figcaption>—Will Roggers, <cite>Shopify</cite></figcaption>
                </figure>

                <h3>pre</h3>

                <pre>Pre: Formatted text</pre>
                <div>Div: Formatted text</div>

                <Code>
                    {`<pre>Formatted text</pre>`}
                </Code>
            </section>
        </div>
    );
}
