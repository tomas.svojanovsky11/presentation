import React from "react"

export function Recommended () {
    return (
        <div>
            <h1>Doporučené</h1>

            <p>
                <a href="https://github.com/miloyip/game-programmer" target="_blank">Game learning path</a>
            </p>
            <p>
                <a href="https://roadmap.sh" target="_blank">Roadmaps</a>
            </p>
            <p>
                <a href="https://www.manning.com/books/good-code-bad-code" target="_blank">Good code and bad code</a>
            </p>
            <p>
                <a href="https://leetcode.com/" target="_blank">Leet code</a>
            </p>

            <p>
                <a href="hhttps://frontendmasters.com/" target="_blank">Frontend masters</a>
            </p>

            <p>
                <a href="https://leetcode.com/" target="_blank">Leet code</a>
            </p>

            <p>
                <a href="https://www.freecodecamp.org/" target="_blank">Freecode camp</a>
            </p>

            <p>
                <a href="https://www.hackerrank.com/" target="_blank">Hackers rank</a>
            </p>

            <p>
                <a href="https://codepip.com/" target="_blank">Code pip</a>
            </p>

            <p>
                <a href="https://100dayscss.com/" target="_blank">CSS 100 days challenge</a>
            </p>

            <p>
                <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript">Mozilla javascript</a>
            </p>

            <p>
                <a href="https://github.com/getify/You-Dont-Know-JS" target="_blank">You don't know JS</a>
            </p>

            <p>
                <a href="https://learngitbranching.js.org/" target="_blank">Learning git</a>
            </p>

            <p>
                <a href="https://www.atlassian.com/git/tutorials/learn-git-with-bitbucket-cloud" target="_blank">Bitbutcket git</a>
            </p>

            <img width={1000} src="images/frontend.png" alt="Front learning path"/>
        </div>
    )
}
