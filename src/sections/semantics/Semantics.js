import React from 'react';

import {Code} from "../../components/code/Code";

export function Semantics(props) {
    return (
        <div>
            <h1>Sémantické značky</h1>

            <ul>
                <li>{`<article>`}</li>
                <li>{`<aside>`}</li>
                <li>{`<header>`}</li>
                <li>{`<nav>`}</li>
                <li>{`<main>`}</li>
                <li>{`<section>`}</li>
            </ul>

            <h3>Porovnání</h3>

            <Code>
                {`<div id="header"></div>
<div class="section">
    <div class="article">
        <div class="figure">
            <img>
            <div class="figcaption"></div>
        </div>
    </div>
</div>
<div id="footer"></div>`}
            </Code>
            
            <Code>
                {`<header></header>
<section>
    <article>
        <figure>
            <img>
            <figcaption></figcaption>
        </figure>
    </article>
</section>
<footer></footer>`}
            </Code>
        </div>
    );
}
