import React from 'react';

import { Code } from "../../components/code/Code";

export function Lists(props) {
    return (
        <div>
            <h1>Seznamy</h1>

            <h3>Číslovaný seznam</h3>

            <ul>
                <li>Dogs</li>
                <li>Cats</li>
                <li>Rabbits</li>
                <li>Mice</li>
            </ul>

            <Code>
                {`<ul>
    <li>Dogs</li>
    <li>Cats</li>
    <li>Rabbits</li>
    <li>Mice</li>
</ul>`}
            </Code>

            <h3>Nečíslovaný seznam</h3>

            <ol>
                <li>Dogs</li>
                <li>Cats</li>
                <li>Rabbits</li>
                <li>Mice</li>
            </ol>

            <Code>
                {`<ol>
    <li>Dogs</li>
    <li>Cats</li>
    <li>Rabbits</li>
    <li>Mice</li>
</ol>`}
            </Code>

            <h3>Odkazy</h3>
            <div>
                <a href="https://developer.mozilla.org/en-US/docs/Web/HTML/Element/ol" target="Blank">Ol seznam</a>
            </div>

            <div>
                <a href="https://developer.mozilla.org/en-US/docs/Web/HTML/Element/ul" target="Blank">Ul seznam</a>
            </div>
        </div>
    );
}
