import React from 'react';

import { Code } from "../../components/code/Code";

export function Tables(props) {
    return (
        <div>
            <h1>Tabulky</h1>

            <h3>Základní verze</h3>

            <table>
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Breed</th>
                        <th>Age</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Bella</td>
                        <td>American Wirehair Cat Breed</td>
                        <td>3</td>
                    </tr>

                    <tr>
                        <td>Kitty</td>
                        <td>Bombay Cat</td>
                        <td>5</td>
                    </tr>

                    <tr>
                        <td>Milo</td>
                        <td>Chartreux Cat Breed</td>
                        <td>7</td>
                    </tr>
                </tbody>
            </table>

            <Code>
                {`<table>
<thead>
    <tr>
        <th>Name</th>
        <th>Breed</th>
        <th>Age</th>
    </tr>
</thead>
<tbody>
    <tr>
        <td>Bella</td>
        <td>American Wirehair Cat Breed</td>
        <td>3</td>
    </tr>

    <tr>
        <td>Kitty</td>
        <td>Bombay Cat</td>
        <td>5</td>
    </tr>

    <tr>
        <td>Milo</td>
        <td>Chartreux Cat Breed</td>
        <td>7</td>
    </tr>
</tbody>
</table>`}
            </Code>

            <h3>Plná verze</h3>

            <table>
                <caption>My cats</caption>
                <colgroup>
                    <col span="2" style={{ "backgroundColor": "red"}}/>
                    <col style={{ "backgroundColor": "yellow" }}/>
                </colgroup>
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Breed</th>
                    <th>Age</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Bella</td>
                    <td>American Wirehair Cat Breed</td>
                    <td>3</td>
                </tr>

                <tr>
                    <td>Kitty</td>
                    <td>Bombay Cat</td>
                    <td>5</td>
                </tr>

                <tr>
                    <td>Milo</td>
                    <td>Chartreux Cat Breed</td>
                    <td>7</td>
                </tr>
                </tbody>
                <tfoot>
                <tr>
                    <td>Average age</td>
                    <td>5   </td>
                    <td></td>
                </tr>
                </tfoot>
            </table>

                <Code>
                    {`<table>
<caption>My cats</caption>
<colgroup>
    <col span="2" style={{ "backgroundColor": "red"}}/>
    <col style={{ "backgroundColor": "yellow" }}/>
</colgroup>
<thead>
    <tr>
        <th>Name</th>
        <th>Breed</th>
        <th>Age</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>Bella</td>
        <td>American Wirehair Cat Breed</td>
        <td>3</td>
    </tr>
    
    <tr>
        <td>Kitty</td>
        <td>Bombay Cat</td>
        <td>5</td>
    </tr>
    
    <tr>
        <td>Milo</td>
        <td>Chartreux Cat Breed</td>
        <td>7</td>
    </tr>
</tbody>
<tfoot>
    <tr>
        <td>Average age</td>
        <td>5   </td>
        <td></td>
    </tr>
</tfoot>`}
                </Code>
        </div>
    );
}
