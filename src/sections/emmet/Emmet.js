import React from "react";

import { Code } from "../../components/code/Code";

export function Emmet () {
    return (
        <main>
            <h1>Emmet</h1>

            <h3>Emmet je plugin do VScode, webstormu atd.</h3>
            <div>Pomocí emmetu můžete psát spoustu HTML za krátkou dobu.</div>

            <p>
                Odkaz: <a href="https://emmet.io/" target="_blank">Emmet</a>
            </p>

            <h1>1.</h1>

            <section>
                <Code showCopy={false}>
                    {`div -> <div></div>`}
                </Code>

                <Code showCopy={false}>
                    {`h1 -> <div></div>`}
                </Code>
            </section>

            <h1>2.</h1>

            <section>
                <Code showCopy={false}>
                    {`blockquote -> <blockquote></blockquote>`}
                </Code>

                <Code showCopy={false}>
                    {`bq -> <blockquote></blockquote>`}
                </Code>

                <Code showCopy={false}>
                    {`btn -> <button></button>`}
                </Code>
            </section>

            <h1>3.</h1>

            <section>
                <Code showCopy={false}>
                    {`div#boo -> <div id="boo"></div>`}
                </Code>

                <Code showCopy={false}>
                    {`div.boo -> <div class="boo"></div>`}
                </Code>

                <Code showCopy={false}>
                    {`div.bat.wiz -> <div class="bat wiz"></div>`}
                </Code>

                <Code showCopy={false}>
                    {`div#bat.wiz -> <div id="bat" class="wiz"></div>`}
                </Code>

                <Code showCopy={false}>
                    {`div#bat.wiz.fiz -> <div id="bat" class="wiz fiz"></div>`}
                </Code>
            </section>

            <h1>4.</h1>

            <section>
                <Code showCopy={false}>
                    {`#bat -> <div id="bat"></div>`}
                </Code>

                <Code showCopy={false}>
                    {`a -> <a href=""></a>`}
                </Code>

                <Code showCopy={false}>
                    {`link -> <link rel="stylesheet" href=""/>`}
                </Code>

                <Code showCopy={false}>
                    {`script -> <script></script>`}
                </Code>

                <Code showCopy={false}>
                    {`script[src] -> <script src=""></script>`}
                </Code>
            </section>

            <h1>5.</h1>

            <section>
                <Code showCopy={false}>
                    {`html[lang] -> <html lang=""></html>`}
                </Code>

                <Code showCopy={false}>
                    {`html[lang="en"] -> <html lang="en"></html>`}
                </Code>
            </section>

            <h1>6.</h1>

            <section>
                <Code showCopy={false}>
                    {`input -> <input type="text"/>`}
                </Code>

                <Code showCopy={false}>
                    {`td[rowspan="2"][colspan="3"] -> <td rowspan="2" colspan="3"></td>`}
                </Code>

                <Code showCopy={false}>
                    {`input[type="button"] -> <input type="button"/>`}
                </Code>

                <Code showCopy={false}>
                    {`input:button -> <input type="button"/>`}
                </Code>

                <Code showCopy={false}>
                    {`input:button -> <input type="checkbox" name="" id=""/>`}
                </Code>
            </section>

            <h1>7.</h1>

            <section>
                <Code showCopy={false}>
                    {`h1{Cat} -> <h1>Cat</h1>`}
                </Code>

                <Code showCopy={false}>
                    {`a{Google} -> <a href="">Google</a>`}
                </Code>

                <Code showCopy={false}>
                    {`h1+p -> <h1></h1><p></p>`}
                </Code>

                <Code showCopy={false}>
                    {`h1+h2+p -> <h1></h1><h2></h2><p></p>`}
                </Code>
            </section>

            <h1>8.</h1>

            <Code showCopy={false}>
                {`div*3 -> <div></div>
         <div></div>
         <div></div>`}
            </Code>

            <Code showCopy={false}>
                {`li*5 -> <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>`}
            </Code>

            <Code showCopy={false}>
                {`h1{Header}*5 -> <h1>Header</h1>
                <h1>Header</h1>
                <h1>Header</h1>
                <h1>Header</h1>
                <h1>Header</h1>`}
            </Code>

            <Code showCopy={false}>
                {`h1{Header $}*5 -> <h1>Header 1</h1>
                  <h1>Header 2</h1>
                  <h1>Header 3</h1>
                  <h1>Header 4</h1>
                  <h1>Header 5</h1>`}
            </Code>

            <Code showCopy={false}>
                {`h1{Header $}*5 -> <li class="item1"></li>
                  <li class="item2"></li>
                  <li class="item3"></li>
                  <li class="item4"></li>
                  <li class="item5"></li>
                  <li class="item6"></li>`}
            </Code>

            <h1>9.</h1>

            <section>
                <Code showCopy={false}>
                    {`head>title -> <head><title></title></head>`}
                </Code>

                <Code showCopy={false}>
                    {`a>img -> <a href=""><img src="" alt=""/></a>`}
                </Code>

                <Code showCopy={false}>
                    {`section>p -> <section>
                <p></p>
             </section>`}
                </Code>

                <Code showCopy={false}>
                    {`section>p*3 -> <section>
                   <p></p>
                   <p></p>
                   <p></p>
               </section>`}
                </Code>

                <Code showCopy={false}>
                    {`(section>p*3)*2 -> <section>
                      <p></p>
                      <p></p>
                      <p></p>
                   </section>
                   <section>
                      <p></p>
                      <p></p>
                      <p></p>
                   </section>`}
                </Code>

                <section>
                    <p></p>
                    <p></p>
                    <p></p>
                </section>
                <section>
                    <p></p>
                    <p></p>
                    <p></p>
                </section>

                <Code showCopy={false}>
                    {`nav>ul>li -> <nav>
               <ul>
                    <li></li>
               </ul>
             </nav>`}
                </Code>
            </section>

            <h1>10.</h1>

            <section>
                <Code showCopy={false}>
                    {`section>h1+p -> <section>
                    <h1></h1>
                    <p></p>
                </section>`}
                </Code>

                <Code showCopy={false}>
                    {`p*2+p>span -> <p></p>
            <p></p>
            <p>
                <span></span>
            </p>`}
                </Code>

                <Code showCopy={false}>
                    {`section>h1+p>span -> <p></p>
                    <p></p>
                    <p>
                        <span></span>
                    </p>`}
                </Code>

                <Code showCopy={false}>
                    {`section>p^section -> <section>
                    <p></p>
                </section>
                <section></section>`}
                </Code>

                <Code showCopy={false}>
                    {`section>p>span^p -> <section>
                <p><span></span></p>
                <p></p>
            </section>`}
                </Code>

                <Code showCopy={false}>
                    {`section>p>a^^section -> <section>
                    <p><a href=""></a></p>
                </section>
                <section></section>`}
                </Code>
            </section>

            <h1>11.</h1>

            <section>
                <Code showCopy={false}>
                    {`table>(thead>tr>th>span)+tbody -> <table>
                    <thead>
                    <tr>
                        <th><span></span></th>
                    </tr>
                    </thead>
                    <tbody></tbody>
                </table>`}
                </Code>

                <Code showCopy={false}>
                    {`(section>(p>span)+p)+section -> <section>
                    <p><span></span></p>
                    <p></p>
                </section>
                <section></section>`}
                </Code>

                <Code showCopy={false}>
                    {`header>ul>li*3+a -> <header>
                    <ul>
                        <li></li>
                        <li></li>
                        <li></li>
                        <a href=""></a></ul>
                </header>`}
                </Code>

                <Code showCopy={false}>
                    {`div.jumbotron.text-center>h1{Welcome}+p -> <div class="jumbotron text-center">
                    <h1>Welcome</h1>
                    <p></p>
                </div>`}
                </Code>

                <Code showCopy={false}>
                    {`ul#pagination>li>a*5.page-link{$} -> <ul id="pagination">
                    <li>
                        <a href="" class="page-link">1</a>
                        <a href="" class="page-link">2</a>
                        <a href="" class="page-link">3</a>
                        <a href="" class="page-link">4</a>
                        <a href="" class="page-link">5</a>
                    </li>
                </ul>`}
                </Code>

                <Code showCopy={false}>
                    {`(div>dl>(dt+dd)*2)+footer>p -> <div>
                        <dl>
                            <dt></dt>
                            <dd></dd>
                            <dt></dt>
                            <dd></dd>
                        </dl>
                    </div>
                    <footer>
                        <p></p>
                    </footer>`}
                </Code>
            </section>
        </main>
    );
}
