import React from "react"
import { Switch, BrowserRouter as Router, Route, Redirect } from "react-router-dom";

import { Menu } from './components/menu/Menu';
import { Recommended } from "./sections/recommended/Recommended";
import { HowWebWorks } from "./sections/howWebWorks/HowWebWorks";
import { HtmlStructure } from "./sections/htmlStructure/HtmlStructure";
import { Emmet } from "./sections/emmet/Emmet";
import { Forms } from "./sections/forms/Forms";
import { HtmlPractice } from "./sections/htmlPractice/HtmlPractice";
import { NotFound } from "./sections/notFound/NotFound";
import { WhatIsFrontendAbout } from "./sections/whatIsFrontendAbount/WhatIsFrontendAbout";
import { WhatDoINeed } from "./sections/whatDoINeed/WhatDoINeed";
import { Flask } from "./sections/flask/Flask";
import { CssIntro } from "./sections/cssIntro/CSSIntro";
import { Bootstrap } from "./sections/boostrap/Bootstrap";
import { Tables } from "./sections/tables/Tables";
import { Blocks } from "./sections/blocks/Blocks";
import { Lists } from "./sections/lists/Lists";
import { Images } from "./sections/images/Images";
import { Inlines } from "./sections/inlines/Inlines";
import { Semantics } from "./sections/semantics/Semantics";

export function Routes () {
    return (
        <Router>
            <div className="container-custom">
                <Menu/>

                <div className="content">
                    <Switch>
                        <Route exact path="/">
                            <Redirect to="/recommended" />
                        </Route>

                        <Route path="/recommended">
                            <Recommended/>
                        </Route>

                        <Route path="/what-do-i-need">
                            <WhatDoINeed/>
                        </Route>

                        <Route path="/what-is-frontend-about">
                            <WhatIsFrontendAbout/>
                        </Route>

                        <Route path="/how-web-works">
                            <HowWebWorks/>
                        </Route>

                        <Route path="/html-structure">
                            <HtmlStructure/>
                        </Route>

                        <Route path="/emmet">
                            <Emmet/>
                        </Route>

                        <Route path="/blocks">
                            <Blocks/>
                        </Route>

                        <Route path="/tables">
                            <Tables/>
                        </Route>

                        <Route path="/forms">
                            <Forms/>
                        </Route>

                        <Route path="/html-practice">
                            <HtmlPractice/>
                        </Route>

                        <Route path="/css-intro">
                            <CssIntro/>
                        </Route>

                        <Route path="/bootstrap">
                            <Bootstrap/>
                        </Route>

                        <Route path="/flask">
                            <Flask/>
                        </Route>

                        <Route path="/lists">
                            <Lists/>
                        </Route>

                        <Route path="/images">
                            <Images/>
                        </Route>

                        <Route path="/inlines">
                            <Inlines/>
                        </Route>

                        <Route path="/semantics">
                            <Semantics/>
                        </Route>

                        <Route path="*">
                            <NotFound/>
                        </Route>
                    </Switch>
                </div>
            </div>
        </Router>
    )
}
